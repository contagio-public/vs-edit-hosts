// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

import * as fs from 'fs';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	//console.log('Congratulations, your extension "vs-edit-hosts" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('extension.editHosts', () => {
		var config=vscode.workspace.getConfiguration('vs-edit-hosts');
		var filePath="";
		filePath=config.get('pathFile');
		
        if(typeof filePath=='undefined' || filePath==""){
            filePath="";
            if (fs.existsSync("C:/Windows/System32/drivers/etc/hosts")) {
                filePath="C:/Windows/System32/drivers/etc/hosts";
            }else if(fs.existsSync("/private/etc/hosts")) {
                filePath="/private/etc/hosts";
            }
        }else{
            if (!fs.existsSync(filePath)) {
                filePath="";
            }
        }
        
        if(filePath!=""){
            var openPath = vscode.Uri.file(filePath);
            vscode.workspace.openTextDocument(openPath).then(doc => {
                vscode.window.showTextDocument(doc);
            });
        }else{
            vscode.window.showInformationMessage('Hosts file not found. Edit Settings!');
        }
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
